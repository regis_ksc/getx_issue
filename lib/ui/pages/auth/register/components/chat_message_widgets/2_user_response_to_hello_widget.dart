import 'package:carupi/ui/widgets/atoms/animated_spacer.dart';
import 'package:carupi/ui/widgets/atoms/chatbox_message_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../register_controller.dart';

class UserResponseToHelloWidget extends GetWidget<RegisterController> {
  @override
  Widget build(BuildContext context) {
    print("Built SelectRegisterTypeButtons");
    return Obx(
      () => Visibility(
        visible: controller.userHasChosenRegisterOption.value,
        child: ChatboxMessageWidget(
          buildWhen: controller.userHasChosenRegisterOption.value,
          message: controller.whatUserWantsWithTheApp,
          leftPadding: Get.width * 0.15,
          isLeftSideMessage: false,
          child: Visibility(
            visible: true,
            child: ASpacer(),
          ),
        ),
      ),
    );
  }
}

class ASpacer extends StatelessWidget {
  final RegisterController controller = Get.find<RegisterController>();

  @override
  Widget build(BuildContext context) {
    return AnimatedSpacer(
      condition: controller.userHasChosenRegisterOption.value,
      initialHeight: Get.height * 0.45,
      finalHeight: Get.height * 0,
      durationInMilliseconds: 0,
    );
  }
}
