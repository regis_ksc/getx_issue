import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../widgets/atoms/animated_spacer.dart';
import '../../../../../widgets/atoms/chatbox_message_widget.dart';
import '../../register_controller.dart';

class CitySelectionWidget extends StatelessWidget {
  final controller = Get.find<RegisterController>();
  bool canBuild = false;
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Visibility(
        visible: controller.userHasChosenRegisterOption.value,
        child: Builder(builder: (_) {
          if (controller.userHasChosenRegisterOption.value) {
            return ChatboxMessageWidget(
                buildWhen: canBuild,
                message: 'Selecione abaixo a cidade que você quer comprar com a Carupi',
                isLeftSideMessage: true,
                child: Obx(() => Visibility(
                      visible: controller.user.value.city != null,
                      child: AnimatedSpacer(
                        condition: !controller.userHasChosenRegisterOption.value,
                        initialHeight: Get.height * 0.35,
                        finalHeight: Get.height * 0,
                        durationInMilliseconds: 0,
                      ),
                    )));
          } else {
            return const SizedBox();
          }
        }),
      ),
    );
  }
}
