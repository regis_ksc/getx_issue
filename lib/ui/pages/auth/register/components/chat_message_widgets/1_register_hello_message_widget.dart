import 'package:carupi/ui/widgets/atoms/animated_spacer.dart';
import 'package:carupi/ui/widgets/atoms/chatbox_message_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../register_controller.dart';

class RegisterHelloMessageWidget extends GetWidget<RegisterController> {
  @override
  Widget build(BuildContext context) {
    print("Built RegisterHelloMessageWidget");
    return ChatboxMessageWidget(
        message: 'Olá! Bem-vindo, como podemos te ajudar hoje?',
        isLeftSideMessage: true,
        rightPadding: Get.width * 0.06,
        child: AnimatedSpacer(
          condition: !Get.find<RegisterController>().userHasChosenRegisterOption.value,
          initialHeight: Get.height * 0.35,
          finalHeight: Get.height * 0,
          durationInMilliseconds: 0,
        ));
  }
}
