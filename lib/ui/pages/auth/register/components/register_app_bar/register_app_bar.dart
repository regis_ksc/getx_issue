import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../widgets/atoms/screen_title.dart';
import '../../register_controller.dart';

class RegisterAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const boxShadow = [
      BoxShadow(
        color: Colors.white,
        blurRadius: 15.0,
        spreadRadius: 20.0,
        offset: Offset(
          10.0,
          10.0,
        ),
      )
    ];
    return Container(
      decoration: const BoxDecoration(boxShadow: boxShadow),
      child: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        elevation: 0,
        flexibleSpace: Column(
          children: <Widget>[
            const SizedBox(height: 35),
            GetBuilder<RegisterController>(
              builder: (_) {
                return ScreenTitle(
                  title: Get.find<RegisterController>().registerPageTitle,
                  backFunction: () async {
                    Navigator.pop(context);
                    await Future.delayed(const Duration(seconds: 2));
                    // global.signUpUser = {};
                    // global.phoneVerified = false;
                    // global.newCarData = {};
                    // global.newCarData2 = {};

                    // global.setQuizpayment(null);
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
