import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../widgets/atoms/gradient_button.dart';
import '../../register_controller.dart';

class ThreeGradientButtonColumn extends GetWidget<RegisterController> {
  final String firstButtonLabel;
  final String secondButtonLabel;
  final String thirdButtonLabel;
  final String firstButtonAsset;
  final String secondButtonAsset;
  final String thirdButtonAsset;
  final Function() firstButtonCallback;
  final Function() secondButtonCallback;
  final Function() thirdButtonCallback;

  ThreeGradientButtonColumn({
    Key key,
    this.firstButtonLabel,
    this.firstButtonCallback,
    this.secondButtonLabel,
    this.thirdButtonLabel,
    this.firstButtonAsset,
    this.secondButtonAsset,
    this.thirdButtonAsset,
    this.secondButtonCallback,
    this.thirdButtonCallback,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: controller.buttonsOpacity.value,
      duration: const Duration(milliseconds: 300),
      child: Column(
        children: <Widget>[
          SizedBox(height: Get.height * 0.015),
          GradientButton(
            text: firstButtonLabel,
            assetPath: firstButtonAsset,
            onTap: firstButtonCallback,
          ),
          SizedBox(height: Get.height * 0.03),
          GradientButton(
            text: secondButtonLabel,
            assetPath: secondButtonAsset,
            onTap: secondButtonCallback,
          ),
          SizedBox(height: Get.height * 0.03),
          GradientButton(
            text: thirdButtonLabel,
            assetPath: thirdButtonAsset,
            onTap: thirdButtonCallback,
          ),
          SizedBox(height: Get.height * 0.015),
        ],
      ),
    );
  }
}
