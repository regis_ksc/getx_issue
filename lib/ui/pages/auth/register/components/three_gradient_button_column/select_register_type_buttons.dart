import 'package:carupi/ui/widgets/atoms/default_button.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../register_controller.dart';
import 'three_gradient_button_column.dart';

class SelectRegisterTypeButtons extends GetWidget<RegisterController> {
  @override
  Widget build(BuildContext context) {
    print("Built SelectRegisterTypeButtons");
    return Obx(() => Visibility(
          visible: !controller.userHasChosenRegisterOption.value,
          replacement: Container(
            margin: EdgeInsets.symmetric(horizontal: Get.height * 0.05),
            child: DefaultButton(
              text: "Concluir",
              function: () {},
              opacity: 1,
            ),
          ),
          child: ThreeGradientButtonColumn(
            firstButtonLabel: "comprar carro",
            firstButtonAsset: "assets/icons/button_car.svg",
            firstButtonCallback: () {
              // controller.fadeFirstSelectionButtons(UserNeedsTo.buyCar);
              /* 
                appController.userQuiz.whatIsYourGoal = "buyer car";
                appController.signUpUser = Map<String, dynamic>();
                widget.indicatedFirstLogin
                    ? Navigator.of(context).push(MaterialPageRoute(builder: (context) => IndicatedBuyChat()))
                    : Navigator.of(context).push(MaterialPageRoute(builder: (context) => BuyChat()));
                */
            },
            secondButtonLabel: 'trocar carro',
            secondButtonAsset: "assets/icons/button_change.svg",
            secondButtonCallback: () {
              // controller.fadeFirstSelectionButtons(UserNeedsTo.changeCar);
              /* 
              appController.signUpUser = Map<String, dynamic>();
              appController.userQuiz.whatIsYourGoal = "change car";
              Navigator.of(context).push(widget.indicatedFirstLogin
                  ? MaterialPageRoute(builder: (context) => IndicatedChangeChat())
                  : MaterialPageRoute(builder: (context) => ChangeChat()));
              */
            },
            thirdButtonLabel: 'vender carro',
            thirdButtonAsset: "assets/icons/button_money.svg",
            thirdButtonCallback: () {
              // controller.fadeFirstSelectionButtons(UserNeedsTo.sellCar);
              /* 
              appController.signUpUser = Map<String, dynamic>();
              appController.userQuiz.whatIsYourGoal = "sell car without exchange";
              Navigator.of(context).push(widget.indicatedFirstLogin
                  ? MaterialPageRoute(builder: (context) => IndicatedSellChat())
                  : MaterialPageRoute(builder: (context) => SellChat()));
              */
            },
          ),
        ));
  }
}
