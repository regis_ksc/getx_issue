import 'package:carupi/ui/widgets/atoms/animated_spacer.dart';
import 'package:carupi/ui/widgets/atoms/chatbox_message_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../register_controller.dart';

class FlowBodyWidget extends StatelessWidget {
  final controller = Get.find<RegisterController>();
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Visibility(
        visible: controller.userHasChosenRegisterOption.value,
        child: ChatboxMessageWidget(
          buildWhen: controller.userHasChosenRegisterOption.value,
          message: "Selecione abaixo a cidade que você quer comprar com a Carupi",
          rightPadding: Get.width * 0.17,
          isLeftSideMessage: true,
          child: Obx(
            () => Visibility(
              visible: true,
              replacement: AnimatedSpacer(
                condition: controller.userHasChosenRegisterOption.value,
                initialHeight: Get.height * 0.3,
                finalHeight: Get.height * 0,
                durationInMilliseconds: 0,
              ),
              child: Container(),
            ),
          ),
        ),
      ),
    );
  }
}
