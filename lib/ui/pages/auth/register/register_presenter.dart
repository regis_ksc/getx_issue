import 'package:get/get.dart';

import 'register_controller.dart';

class RegisterPresenter extends GetxController {
  static get controller => Get.find<RegisterController>();
}
