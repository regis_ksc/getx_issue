import 'package:carupi/ui/widgets/atoms/chatbox_message_widget.dart';
import 'package:get/get.dart';

import 'register_controller.dart';
import 'register_presenter.dart';

class RegisterBinding implements Bindings {
  @override
  void dependencies() {
    // Get.put(ChatboxMessageController(), permanent: false);
    // Get.create(() => ChatboxMessageController(), permanent: false);
    Get.lazyPut<RegisterController>(() => RegisterController());
    Get.lazyPut<RegisterPresenter>(() => RegisterPresenter());
  }
}
