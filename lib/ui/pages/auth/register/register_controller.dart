import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../modules/auth/domain/entities/user/user_entity.dart';
import 'components/chat_message_widgets/1_register_hello_message_widget.dart';
import 'components/three_gradient_button_column/select_register_type_buttons.dart';
import 'register_presenter.dart';

enum UserNeedsTo { beWelcomed, buyCar, sellCar, changeCar }

class RegisterController extends GetxController {
  final presenter = Get.find<RegisterPresenter>();

  RxList<Widget> registerFlowWidgets = <Widget>[].obs;
  int get listLength => registerFlowWidgets.length;
  addWidgetToFlowAfter(int milliseconds, List<Widget> widgets, {final bool isSecondToLast = true}) {
    Future.delayed(Duration(milliseconds: milliseconds), () {
      for (final Widget widget in widgets) {
        if (isSecondToLast && listLength > 1) {
          registerFlowWidgets.insert(listLength - 2, widget);
        } else {
          registerFlowWidgets.add(widget);
        }
      }
    });
  }

  @override
  void onInit() {
    super.onInit();
    addWidgetToFlowAfter(0, [Container(color: Colors.red, child: const SizedBox(height: 30))], isSecondToLast: false);
    addWidgetToFlowAfter(200, [RegisterHelloMessageWidget()], isSecondToLast: false);
    // addWidgetToFlowAfter(1000, [SelectRegisterTypeButtons()]);
  }

  final buttonsOpacity = 0.0.obs;
  final userHasChosenRegisterOption = false.obs;

  Rx<UserEntity> user = const UserEntity().obs;
  UserNeedsTo userNeeds = UserNeedsTo.beWelcomed;
  String get whatUserWantsWithTheApp {
    switch (userNeeds) {
      case UserNeedsTo.beWelcomed:
        break;
      case UserNeedsTo.buyCar:
        registerPageTitle = "Comprar";
        return "Eu quero comprar um carro";
        break;
      case UserNeedsTo.sellCar:
        registerPageTitle = "Vender";
        return "Eu quero vender meu carro";
        break;
      case UserNeedsTo.changeCar:
        registerPageTitle = "Trocar";
        return "Eu quero trocar de carro";
        break;
    }
    return "";
  }

  ScrollController scroll = ScrollController();
  String registerPageTitle = "";

  /* fadeFirstSelectionButtons(UserNeedsTo whatUserNeeds) {
    buttonsOpacity.value = 0;
    Future.delayed(const Duration(milliseconds: 600), () {
      addWidgetsToList([UserResponseToHelloWidget()]);
      userHasChosenRegisterOption.value = true;
      userNeeds = whatUserNeeds;
    });
    print("listLength:$listLength");
  } */

  /* scrollPage() {
    scroll.animateTo(
      scroll.position.maxScrollExtent,
      duration: const Duration(milliseconds: 600),
      curve: Curves.easeIn,
    );
  } */

  // * UI vars
  final List<BoxShadow> boxShadows = [
    const BoxShadow(
      color: Colors.white,
      blurRadius: 40.0,
      spreadRadius: 60.0,
      offset: Offset(10.0, 10.0),
    )
  ];
}
