import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'components/register_app_bar/register_app_bar.dart';
import 'register_controller.dart';

class RegisterView extends StatelessWidget {
  static RegisterController get controller => Get.find<RegisterController>();
  // final appController = Get.find<AppController>();

  static PreferredSize get appBar => PreferredSize(preferredSize: const Size.fromHeight(55), child: RegisterAppBar());

  @override
  Widget build(BuildContext context) {
    print("List length: ${controller.registerFlowWidgets.length}");
    return Scaffold(
      appBar: appBar,
      body: Obx(
        () => ListView(
          physics: const BouncingScrollPhysics(),
          padding: EdgeInsets.zero,
          controller: controller.scroll,
          shrinkWrap: true,
          children: controller.registerFlowWidgets.toList(),
        ),
      ),
    );
  }
}
