import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../core/utils/constants/app_theme.dart';

class SearchField extends StatelessWidget {
  final TextEditingController controller;

  const SearchField({
    @required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(horizontal: Get.width * 0.13),
        child: Container(
          height: Get.width * 0.108,
          child: Material(
              elevation: 20.0,
              borderRadius: const BorderRadius.all(Radius.circular(25)),
              shadowColor: Colors.black.withOpacity(0.9),
              color: Colors.white,
              child: Row(
                children: [
                  Expanded(
                    child: SvgPicture.asset(
                      'assets/icons/search_search.svg',
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: Center(
                      child: TextField(
                        controller: controller,
                        cursorColor: Colors.black,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                            hintText: 'Busque por modelo ou marca',
                            hintStyle: const TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w400,
                            ),
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.symmetric(
                              horizontal: Get.width * 0.02,
                            )),
                      ),
                    ),
                  ),
                  Expanded(
                      child: Visibility(
                    visible: controller.text == null,
                    replacement: const SizedBox(),
                    child: GestureDetector(
                      onTap: () {},
                      child: SvgPicture.asset(
                        "assets/icons/closeIcon.svg",
                        color: AppColors.laError,
                      ),
                    ),
                  ))
                ],
              )),
        ));
  }
}
