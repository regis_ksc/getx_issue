import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class ScreenTitle extends StatelessWidget {
  final bool backButton;
  final bool smallButton;
  final Widget trailing;
  final Function() trailingFunction;
  final Function() backFunction;
  final Color backColor;
  final Color titleColor;
  final double fontSize;
  final double horizontalPadding;
  final String title;

  const ScreenTitle({
    Key key,
    @required this.title,
    this.trailing,
    this.trailingFunction,
    this.backFunction,
    this.backColor,
    this.titleColor,
    this.fontSize,
    this.backButton = true,
    this.smallButton = true,
    this.horizontalPadding = 16,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: horizontalPadding),
      child: Row(
        children: <Widget>[
          Visibility(
            visible: backButton,
            replacement: const SizedBox(width: 20),
            child: InkWell(
              splashColor: Colors.transparent,
              onTap: backFunction ?? () {},
              child: SizedBox(
                height: 40,
                width: 40,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Center(
                      child: SvgPicture.asset(
                        "assets/icons/new_back_icon.svg",
                        color: Colors.grey[600],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: fontSize ?? 16,
                color: titleColor,
                fontWeight: FontWeight.w600,
                letterSpacing: 0.22,
              ),
            ),
          ),
          Visibility(
            visible: trailing != null,
            replacement: const SizedBox(width: 20),
            child: GestureDetector(
              onTap: trailingFunction,
              child: trailing,
            ),
          ),
        ],
      ),
    );
  }
}
