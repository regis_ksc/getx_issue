import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
// import 'package:flutter_svg/flutter_svg.dart';

class GradientButton extends StatelessWidget {
  final Function() onTap;
  final String text;
  final String assetPath;
  final Color startColor;
  final Color endColor;
  final Alignment startAlignment;
  final Alignment endAlignment;

  const GradientButton({
    Key key,
    @required this.text,
    @required this.onTap,
    this.startColor,
    this.endColor,
    this.startAlignment,
    this.endAlignment,
    this.assetPath,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 48.5,
          width: 250.57,
          child: RaisedButton(
            onPressed: onTap,
            elevation: 0,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
            padding: const EdgeInsets.all(0.0),
            child: Ink(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [startColor ?? const Color(0xff5643CE), startColor ?? const Color(0xffA146DD)],
                    begin: startAlignment ?? Alignment.centerLeft,
                    end: endAlignment ?? Alignment.centerRight,
                  ),
                  borderRadius: BorderRadius.circular(37.0)),
              child: Container(
                constraints: const BoxConstraints(maxWidth: 250.57, minHeight: 50.0),
                alignment: Alignment.centerLeft,
                child: Row(
                  children: <Widget>[
                    const SizedBox(width: 25),
                    Visibility(
                      visible: assetPath != null,
                      child: Container(
                        width: 20,
                        child: SvgPicture.asset(assetPath),
                      ),
                    ),
                    Visibility(visible: assetPath != null, child: const SizedBox(width: 10)),
                    Text(text,
                        textAlign: TextAlign.start,
                        style: const TextStyle(
                          fontFamily: "Gilroy",
                          fontSize: 12,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        )),
                    const Expanded(child: SizedBox()),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
