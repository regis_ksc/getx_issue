import 'package:flutter/material.dart';

class AnimatedSpacer extends StatelessWidget {
  final double initialHeight;
  final double finalHeight;
  final double initialWidth;
  final double finalWidth;
  final int durationInMilliseconds;
  final bool condition;
  final bool visible;
  final Widget child;

  const AnimatedSpacer({
    Key key,
    this.initialHeight = 0,
    this.finalHeight = 0,
    this.initialWidth = 0,
    this.finalWidth = 0,
    this.durationInMilliseconds = 500,
    this.condition = false,
    this.child,
    this.visible = true,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: visible,
      child: AnimatedContainer(
        duration: Duration(milliseconds: durationInMilliseconds),
        height: condition ? initialHeight : finalHeight,
        width: condition ? initialWidth : finalWidth,
      ),
    );
  }
}
