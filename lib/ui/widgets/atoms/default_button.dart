import 'package:la/core/config/app_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/utils/constants/app_theme.dart';
import 'progress_indicator.dart';

class DefaultButton extends StatelessWidget {
  final Function() function;
  final String text;
  final Widget arrow;
  final double opacity;
  final bool backGroundOtherColor;
  final Color backGroundNewColor;
  final Color textNewColor;
  final Color progressColor;
  final double radius;
  final bool chatButton;
  final TextStyle textStyle;
  final Color borderColor;
  final double lettering;
  final Key key;
  final bool swipe;

  static bool get isLoading => Get.find<AppController>().isLoading.value;

  const DefaultButton({
    @required this.function,
    @required this.text,
    this.arrow,
    @required this.opacity,
    this.lettering,
    this.swipe = false,
    this.backGroundOtherColor,
    this.backGroundNewColor,
    this.textNewColor,
    this.progressColor,
    this.radius = 37,
    this.chatButton = false,
    this.textStyle,
    this.borderColor,
    this.key,
  });

  @override
  Widget build(BuildContext context) {
    bool otherColor = backGroundOtherColor;
    final TextStyle defaultTextStyle = TextStyle(
      fontSize: chatButton ? 14 : 16,
      color: textNewColor ?? AppColors.white,
      fontFamily: "Gilroy",
      fontWeight: chatButton ? FontWeight.w400 : FontWeight.bold,
    );
    if (backGroundOtherColor == null) otherColor = false;
    return Visibility(
      visible: isLoading,
      replacement: IgnorePointer(
        ignoring: isLoading,
        child: Container(
          height: 56,
          child: Opacity(
            opacity: opacity,
            child: RaisedButton(
              elevation: 0,
              color: backGroundNewColor ?? AppColors.appColor,
              onPressed: function,
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius ?? 15)),
              child: Visibility(
                visible: arrow == null,
                replacement: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      text,
                      style: defaultTextStyle,
                    ),
                    arrow ?? Container(),
                  ],
                ),
                child: Text(
                  text,
                  style: textStyle ?? defaultTextStyle,
                ),
              ),
            ),
          ),
        ),
      ),
      child: const LoadingIndicator(),
    );
  }
}
