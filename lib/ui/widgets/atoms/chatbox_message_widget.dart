import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

import '../../../core/utils/constants/app_theme.dart';

class ChatboxMessageController extends GetxController {
  final wholeWidgetOpacity = 0.obs;
  final messageOpacity = 0.obs;
  final isTyping = true.obs;

  @override
  void onInit() {
    super.onInit();
    wholeWidgetOpacity.value = 1;
    Future.delayed(const Duration(milliseconds: 700), () {
      isTyping.value = false;
      Future.delayed(const Duration(milliseconds: 700), () => messageOpacity.value = 1);
    });
  }
}

class ChatboxMessageWidget extends GetWidget<ChatboxMessageController> {
  final double leftPadding;
  final double rightPadding;
  final double height;
  final int durationInMilliseconds;
  final String message;
  final FontWeight fontWeight;
  final bool isLeftSideMessage;
  final bool buildWhen;
  final Widget child;

  ChatboxMessageWidget({
    Key key,
    @required this.isLeftSideMessage,
    this.leftPadding,
    this.rightPadding,
    this.height,
    this.message,
    this.fontWeight,
    this.durationInMilliseconds,
    this.child,
    this.buildWhen = true,
  })  : assert(isLeftSideMessage != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: controller.wholeWidgetOpacity.value.toDouble(),
      duration: const Duration(milliseconds: 300),
      child: Padding(
        padding: EdgeInsets.only(
          left: leftPadding ?? 0,
          right: rightPadding ?? 0,
          top: Get.height * 0.01,
        ),
        child: Column(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  alignment: isLeftSideMessage ? Alignment.centerLeft : Alignment.centerRight,
                  padding: EdgeInsets.only(
                    right: isLeftSideMessage ? 60 : 15,
                    left: !isLeftSideMessage ? 60 : 0,
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: isLeftSideMessage ? MainAxisAlignment.start : MainAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        // color: Colors.blueGrey,
                        alignment: Alignment.bottomRight,
                        padding: isLeftSideMessage
                            ? const EdgeInsets.only(right: 10, left: 15)
                            : const EdgeInsets.only(left: 16, right: 16, top: 10, bottom: 10),
                        child: Visibility(
                          visible: isLeftSideMessage,
                          child: CarupiAvatarBadge(),
                        ),
                      ),
                      Expanded(
                        child: Obx(() => Visibility(
                              visible: controller.isTyping.value && isLeftSideMessage,
                              replacement: Obx(() => AnimatedOpacity(
                                    opacity: controller.messageOpacity.value.toDouble(),
                                    duration: const Duration(milliseconds: 200),
                                    child: GetBuilder(
                                        builder: (_) => MessageSizedBox(
                                              controller: controller,
                                              text: message,
                                              fontWeight: fontWeight,
                                              isLeftSideMessage: isLeftSideMessage,
                                            )),
                                  )),
                              child: FlareIsTypingAnimationWidget(),
                            )),
                      )
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(height: Get.height * 0.04),
            child ?? Container(),
          ],
        ),
      ),
    );
  }
}

class CarupiAvatarBadge extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 30,
      width: 30,
      child: SvgPicture.asset("assets/icons/la_round_icon.svg"),
    );
  }
}

class MessageSizedBox extends StatelessWidget {
  const MessageSizedBox({
    @required this.text,
    @required this.fontWeight,
    @required this.isLeftSideMessage,
    this.controller,
  });

  final String text;
  final ChatboxMessageController controller;
  final bool isLeftSideMessage;
  final FontWeight fontWeight;

  @override
  Widget build(BuildContext context) {
    return Builder(builder: (_) {
      if (!controller.isTyping.value || !isLeftSideMessage) {
        return Container(
          padding: isLeftSideMessage
              ? const EdgeInsets.all(16)
              : const EdgeInsets.only(left: 16, right: 16, top: 11, bottom: 11),
          decoration: ShapeDecoration(
            color: isLeftSideMessage ? AppColors.chatMessageContainer : AppColors.appColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                bottomLeft: isLeftSideMessage ? const Radius.circular(4) : const Radius.circular(20),
                bottomRight: isLeftSideMessage ? const Radius.circular(20) : const Radius.circular(4),
                topLeft: const Radius.circular(20),
                topRight: const Radius.circular(20),
              ),
            ),
          ),
          child: Text(
            text,
            textAlign: isLeftSideMessage ? TextAlign.left : TextAlign.right,
            softWrap: true,
            style: TextStyle(
              fontSize: 14,
              fontWeight: fontWeight,
              color: isLeftSideMessage ? AppColors.black : AppColors.white,
            ),
          ),
        );
      } else {
        return const SizedBox();
      }
    });
  }
}

class FlareIsTypingAnimationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomLeft,
      width: 50,
      child: const SizedBox(
        height: 15,
        width: 30,
        child: FlareActor(
          "assets/animations/typing_status.flr",
          animation: "typing",
        ),
      ),
    );
  }
}
