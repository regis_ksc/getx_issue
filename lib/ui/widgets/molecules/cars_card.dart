import 'package:la/core/utils/constants/app_theme.dart';
import 'package:la/ui/widgets/atoms/pourple_label.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

class CarsCard extends StatelessWidget {
  final bool favorite;
  final String modelCar;
  final String makerCar;
  final String versionCar;
  final String year;
  final String km;
  final String price;

  const CarsCard({
    @required this.favorite,
    @required this.modelCar,
    @required this.makerCar,
    @required this.versionCar,
    @required this.year,
    @required this.km,
    @required this.price,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: Get.width * 0.04, top: Get.width * 0.04),
      child: GestureDetector(
        onTap: () {},
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              alignment: Alignment.topRight,
              padding: EdgeInsets.all(Get.width * 0.023),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4.0),
                image: const DecorationImage(
                    image: AssetImage(
                      'assets/images/car_test.jpeg',
                    ),
                    fit: BoxFit.cover),
              ),
              height: Get.height * 0.18,
              width: Get.height * 0.27,
              child: InkWell(
                onTap: () {},
                child: Container(
                  width: 18,
                  height: 18,
                  child: Visibility(
                    visible: favorite,
                    replacement: SvgPicture.asset(
                      "assets/icons/not_favorite.svg",
                      color: Colors.white,
                    ),
                    child: const FlareActor(
                      "assets/animations/favorite.flr",
                      animation: "Fav on",
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: Get.height * 0.01),
            Text(
              modelCar,
              style: const TextStyle(fontSize: 14, letterSpacing: 0.225, fontWeight: FontWeight.w700),
            ),
            Text(
              makerCar,
              style:
                  const TextStyle(color: Colors.grey, fontSize: 14, letterSpacing: 0.225, fontWeight: FontWeight.w600),
            ),
            SizedBox(height: Get.height * 0.002),
            Container(
              width: 160,
              height: 30,
              child: Text(
                versionCar,
                maxLines: 2,
                textAlign: TextAlign.start,
                overflow: TextOverflow.ellipsis,
                style:
                    TextStyle(color: Colors.grey[700], fontSize: 12, letterSpacing: 0.225, fontWeight: FontWeight.w400),
              ),
            ),
            Row(
              children: [
                PourpleLabel(title: year),
                SizedBox(
                  width: Get.width * 0.02,
                ),
                PourpleLabel(title: km),
              ],
            ),
            SizedBox(
              height: Get.height * 0.008,
            ),
            Text(
              price,
              maxLines: 1,
              textAlign: TextAlign.start,
              style:
                  TextStyle(color: AppColors.appColor, fontSize: 20, letterSpacing: 0.225, fontWeight: FontWeight.w700),
            ),
          ],
        ),
      ),
    );
  }
}
