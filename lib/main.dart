import 'package:flare_flutter/flare_cache.dart';
import 'package:flutter/material.dart';

import 'core/config/app_widget.dart';
import 'core/utils/flare/flare_warmup.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  FlareCache.doesPrune = false;
  warmupAnimations().then((_) => runApp(AppWidget()));
}
