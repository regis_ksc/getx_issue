import 'dart:ui';

import 'package:flare_flutter/flare_cache.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:get/get.dart';
import 'package:package_info/package_info.dart';

class AppController extends GetxController {
  @override
  // ignore: always_declare_return_types
  onInit() {
    getPackageInfo();
    KeyboardVisibility.onChange.listen(
      (bool visible) => setKeyBoardIsShown(visible),
    );
    FlareCache.doesPrune = false;
  }

  bool production = false;
  String appVersion;
  static List<String> supportedLanguages = ["en", "pt", "es"];
  Locale get locale => supportedLanguages.contains(window.locale.languageCode) ? window.locale : const Locale("en", "");

  final isLoading = false.obs;

  static bool get bigHeight => Get.height > 800;
  static bool get bigWidth => Get.width > 390;
  static bool get smallHeight => Get.height < 700;
  static bool get smallWidth => Get.width < 376;

  PackageInfo packageInfo;
  Future<void> getPackageInfo() async {
    packageInfo = await PackageInfo.fromPlatform();
    appVersion = packageInfo.version;
  }

  FocusScopeNode currentFocus;

  /// Pass FocusScope.of(context)
  // ignore: always_declare_return_types
  setCurrentFocus() {
    currentFocus = FocusScope.of(Get.context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      currentFocus.focusedChild.unfocus();
    }
  }

  bool keyboardIsShown = false;
  // ignore: always_declare_return_types
  setKeyBoardIsShown(bool value) {
    keyboardIsShown = value;
  }
}
