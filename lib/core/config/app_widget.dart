import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../utils/constants/app_theme.dart';
import 'app_controller.dart';
import 'app_routes.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appController = Get.put(AppController(), permanent: true);
    return GetMaterialApp(
      title: 'L',
      theme: themeData,
      debugShowCheckedModeBanner: false,
      locale: appController.locale,
      defaultTransition: Transition.leftToRightWithFade,
      initialRoute: '/',
      getPages: AppRoutes.routes,
    );
  }
}
