import 'package:la/core/config/app_binding.dart';
import 'package:la/ui/pages/auth/register/register_binding.dart';
import 'package:la/ui/pages/auth/register/register_view.dart';
import 'package:get/get.dart';

abstract class Routes {
  static const initial = '/';
  static const onboarding = '/onboarding';
  static const home = '/home';
  static const login = '/login';
  static const register = '/register';
}

// ignore: avoid_classes_with_only_static_members
abstract class AppRoutes {
  //
  static List<GetPage> routes = [
    GetPage(
      name: Routes.initial,
      page: () => RegisterView(),
      transition: Transition.noTransition,
      bindings: [RegisterBinding(), AppBinding()],
    ),
  ];
}
