import 'package:flare_flutter/asset_provider.dart';
import 'package:flare_flutter/flare_cache.dart';
import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/services.dart';

Future<void> warmupAnimations() async {
  final AssetProvider assetProvider0 = AssetFlare(bundle: rootBundle, name: "assets/animations/search.flr");
  final AssetProvider assetProvider1 = AssetFlare(bundle: rootBundle, name: "assets/animations/car.flr");
  final AssetProvider assetProvider2 = AssetFlare(bundle: rootBundle, name: "assets/animations/partner_animation.flr");
  final AssetProvider assetProvider3 = AssetFlare(bundle: rootBundle, name: "assets/animations/profile.flr");
  final AssetProvider assetProvider4 = AssetFlare(bundle: rootBundle, name: "assets/animations/favorite.flr");
  final AssetProvider assetProvider5 = AssetFlare(bundle: rootBundle, name: "assets/animations/typing_status.flr");
  await cachedActor(assetProvider0);
  await cachedActor(assetProvider1);
  await cachedActor(assetProvider2);
  await cachedActor(assetProvider3);
  await cachedActor(assetProvider4);
  await cachedActor(assetProvider5);
}
