import 'package:flutter/material.dart';

final ThemeData themeData = ThemeData(
  fontFamily: 'Gilroy',
  cursorColor: Colors.black,
  scaffoldBackgroundColor: Colors.white,
  brightness: Brightness.light,
  primaryColor: AppColors.appColor,
  primaryColorBrightness: Brightness.light,
  accentColorBrightness: Brightness.light,
  buttonColor: AppColors.appColor,
  backgroundColor: Colors.white,
  textTheme: const TextTheme(
    button: TextStyle(fontSize: 16.0, color: Colors.white, fontFamily: "Gilroy", fontWeight: FontWeight.bold),
  ),
);

final ThemeData darkThemeData = ThemeData(
  fontFamily: 'Gilroy',
  cursorColor: Colors.black,
  scaffoldBackgroundColor: Colors.black,
  brightness: Brightness.dark,
  primaryColor: AppColors.appColor,
  backgroundColor: Colors.black,
  primaryColorBrightness: Brightness.dark,
  accentColorBrightness: Brightness.dark,
  buttonColor: AppColors.appColor,
  textTheme: const TextTheme(
    button: TextStyle(fontSize: 16.0, color: Colors.white, fontFamily: "Gilroi", fontWeight: FontWeight.bold),
  ),
);

//BACK ICON
const double backIconTopDistance = 22;
const double backIconLeftDistance = 16;
final imageIcon = Image.asset(
  "assets/icons/backIcon.png",
);
final imageIcon2 = Image.asset(
  "assets/icons/backIconNew.png",
);

const InputDecoration textFieldDecoration = InputDecoration(
  // ignore: avoid_redundant_argument_values
  focusedBorder:
      // ignore: avoid_redundant_argument_values
      UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
  enabledBorder: UnderlineInputBorder(
    // ignore: avoid_redundant_argument_values
    borderSide: BorderSide(color: Colors.black),
  ),
);

final InputDecoration textFieldDecoration2 = InputDecoration(
  // ignore: prefer_const_constructors
  // ignore: avoid_redundant_argument_values
  focusedBorder:
      // ignore: avoid_redundant_argument_values
      const UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
  enabledBorder: UnderlineInputBorder(
    borderSide: BorderSide(
      color: AppColors.laError,
    ),
  ),
);

// ignore: avoid_classes_with_only_static_members
abstract class AppColors {
  // Apps
  static Color appColor = const Color(0xFFFF6F5E);
  static Color laGrey = const Color.fromARGB(1000, 151, 151, 151);
  static Color white = Colors.white;
  static Color black = Colors.black;

  // Events
  static Color laError = const Color.fromARGB(1000, 255, 33, 82);
  static Color warningColor = const Color.fromARGB(1000, 255, 150, 85);
  static Color aproved = const Color.fromARGB(1000, 11, 234, 207);
  static Color newChips = const Color(0xFF6F4BAC);

  // Components
  static Color otherButtonColor = const Color.fromARGB(1000, 103, 103, 103);
  static Color cardBackgrondColor = const Color.fromARGB(1000, 236, 236, 236);
  static Color chatMessageContainer = const Color.fromARGB(1000, 237, 237, 237);
  static Color dividerColor = const Color.fromARGB(1000, 142, 142, 142);
  static Color boxShadow = const Color.fromARGB(60, 0, 0, 0);
  static Color multipleGarageButtonsColor1 = const Color.fromARGB(1000, 111, 75, 172);
  static Color storeBar = const Color.fromARGB(1000, 132, 138, 255);
  static Color greyBackMoney = const Color.fromARGB(100, 250, 250, 250);
  static Color backOfferButton = const Color.fromARGB(1000, 151, 204, 199);
  static Color acceptOfferButton = const Color.fromARGB(1000, 61, 242, 144);

  // Texts
  static Color hintText = const Color.fromARGB(650, 255, 255, 255);
  static Color textProfile = const Color.fromARGB(130, 0, 0, 0);
  static Color textEmptyScreen2 = const Color.fromARGB(1000, 83, 83, 83);
  static Color textEmptyScreen = const Color.fromARGB(1000, 138, 148, 166);
  static Color chatTextInput = const Color.fromARGB(1000, 171, 167, 190);
  static Color chatTime = const Color.fromARGB(1000, 203, 202, 209);
  static Color backTextColor = const Color.fromARGB(1000, 131, 131, 131);

  // Icons
  static Color chatTopIcons = const Color.fromARGB(1000, 200, 199, 207);
  static Color mediaBackIcon = const Color.fromARGB(1000, 195, 199, 207);

  // Repeated
  static Color pdwDontMatchColor = const Color.fromARGB(1000, 255, 33, 82); // laError

}
